import { createElement, EventEmitter } from './helpers';

class View extends EventEmitter{
    constructor () {
        super();

        this.form = document.getElementById('todo-form');
        this.input = document.getElementById('add-input');
        this.list = document.getElementById('todo-list');

        this.form.addEventListener('submit', this.handleAdd.bind(this));
    }

    createElement(todo) {
        const checkbox = createElement('input', { type: 'checkbox', className: 'checkbox', checked: todo.completed ? 'checked'
        : ''});
        const label = createElement('label', {className: 'title'}, todo.title);
        const editInput = createElement('input', {type: 'text', className: 'textfield'});
        const editBtn = createElement('button', {className: 'edit'}, 'Change');
        const removeBtn = createElement('button', {className: 'remove'}, 'Remove');
        const item = createElement('li', {className: `todo-item${todo.completed ? ' completed' : ''}`},
            checkbox, label, editBtn, editInput, removeBtn);
        item.setAttribute('data-id', todo.id)

        return this.addEventListeners(item);
    }

    addEventListeners(item) {
        const checkbox = item.querySelector('.checkbox');
        const editBtn = item.querySelector('button.edit');
        const removeBtn = item.querySelector('button.remove');

        checkbox.addEventListener('change', this.handleToggle.bind(this));
        editBtn.addEventListener('click', this.handleEdit.bind(this));
        removeBtn.addEventListener('click', this.handleRemove.bind(this));

        return item;
    }

    handleToggle({target}) {
        const listItem = target.parentNode;
        const id = listItem.getAttribute('data-id');
        const completed = target.checked;

        this.emit('toggle', { id, completed })
    }

    handleEdit({target}) {
        const listItem = target.parentNode;
        const id = listItem.getAttribute('data-id');
        const label = listItem.querySelector('.title');
        const input = listItem.querySelector('.textfield');
        const editBtn = listItem.querySelector('button.edit');
        const title = input.value;
        const isEditing = listItem.classList.contains('editing');

        if (isEditing) {
            this.emit('edit', {id, title});
        } else {
            input.value = label.textContent;
            editBtn.textContent = 'Save';
            listItem.classList.add('editing');
        }

    }

    handleRemove({target}) {
        const listItem = target.parentNode;
        const id = listItem.getAttribute('data-id');
        
        this.emit('remove', id);
    }

    handleAdd(e) {
        e.preventDefault();
        
        if (!this.input.value) {
            alert('Write task');
        } else {
            const value = this.input.value;
            this.emit('add', value);
        }
    }

    findListItem(id) {
        return this.list.querySelector(`[data-id="${id}"]`);
    }

    addItem(todo) {
        const listItem = this.createElement(todo);

        this.input.value = '';
        this.list.appendChild(listItem);
    }

    toggleItem(todo) {
        const listItem = this.findListItem(todo.id);
        const checkbox = listItem.querySelector('.checkbox');

        checkbox.checked = todo.completed;

        if (todo.completed) {
            listItem.classList.add('completed');
        } else {
            listItem.classList.remove('completed');
        }
    }

    editItem(todo) {
        const listItem = this.findListItem(todo.id);
        const label = listItem.querySelector('.title');
        const input = listItem.querySelector('.textfield');
        const editBtn = listItem.querySelector('button.edit');
        
        label.textContent = todo.title;
        editBtn.textContent = 'Change';
        listItem.classList.remove('editing');
    }

    removeItem(id) {
        const listItem = this.findListItem(id);
        this.list.removeChild(listItem);        
    }
}

export default View;